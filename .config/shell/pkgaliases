#!/bin/sh
## p: a common package manager interface based on https://github.com/andschwa/shell/blob/master/.shell/packages.sh

# p:    the package manager
# pse:  search packages
# par:  auto-remove unused dependencies
# pbd:  list build dependencies
# pcl:  clean caches
# pdep: show dependencies
# pi:   install package
# pii:  install package unattended
# pin:  show package info
# pif:  install from package file
# pr:   remove package
# pre:  reinstall package
# pup:  update repositories and show upgrades
# pug:  install upgrades
# puu:  update repositories and upgrade
# pla:  list all installed packets chronologically

PSUDO='sudo'
if command -v aptitude >/dev/null 2>&1; then
		alias p='aptitude'
		alias pse='p search'
		alias par="$PSUDO apt-get autoremove"
		alias pbd="$PSUDO aptitude build-dep"
		alias pcl="$PSUDO aptitude clean"
		alias pdep='pin'
		alias pi="$PSUDO aptitude install"
		alias pii="$PSUDO aptitude -y install"
		alias pin='p show'
		alias pif="$PSUDO dpkg -i"
		alias pr="$PSUDO aptitude purge"
		alias pre="$PSUDO aptitude reinstall"
		alias pup="$PSUDO aptitude update"
		alias pug="$PSUDO aptitude upgrade"
		alias puu='pup && pug'
		alias pla='ls /var/lib/dpkg/info/ -ltra --time-style=long-iso | grep .list$'" | awk '"'{print $6 " " $7 " " $8 " " $9}'"' | awk -F. '{print$1}'"
elif command -v apt >/dev/null 2>&1; then
		alias p='apt'
		alias pse='apt search'
		alias par="$PSUDO p autoremove"
		alias pbd="$PSUDO p build-dep"
		alias pcl="$PSUDO p clean"
		alias pdep='pin'
		alias pi="$PSUDO p install"
		alias pii="$PSUDO apt -y install"
		alias pin='apt show'
		alias pif="$PSUDO dpkg -i"
		alias pr="$PSUDO p purge"
		alias pre="$PSUDO p install --reinstall"
		alias pup="$PSUDO p update"
		alias pug="$PSUDO p upgrade"
		alias puu='pup && pug'
		alias pla='ls /var/lib/dpkg/info/ -ltra --time-style=long-iso | grep .list$'" | awk '"'{print $6 " " $7 " " $8 " " $9}'"' | awk -F. '{print$1}'"
elif command -v apt-get >/dev/null 2>&1; then
		alias p='apt-get'
		alias pse='apt-cache search'
		alias par="$PSUDO apt-get autoremove"
		alias pbd="$PSUDO apt-get build-dep"
		alias pcl="$PSUDO apt-get clean"
		alias pdep='pin'
		alias pi="$PSUDO apt-get install"
		alias pii="$PSUDO apt-get -y install"
		alias pin='apt-cache show'
		alias pif="$PSUDO dpkg -i"
		alias pr="$PSUDO apt-get purge"
		alias pre="$PSUDO apt-get install --reinstall"
		alias pup="$PSUDO apt-get update"
		alias pug="$PSUDO apt-get upgrade"
		alias puu='pup && pug'
		alias pla='ls /var/lib/dpkg/info/ -ltra --time-style=long-iso | grep .list$'" | awk '"'{print $6 " " $7 " " $8 " " $9}'"' | awk -F. '{print$1}'"
elif command -v brew >/dev/null 2>&1; then
		alias p='brew'
		alias pse='p search'
		alias par='echo brew cannot auto-remove unused dependencies'
		alias pbd='pdep' # not exactly build dependencies
		alias pcl='p cleanup && brew prune'
		alias pdep='p deps --tree'
		alias pi='p install'
		alias pii='p install'
		alias pin='p info'
		alias pif='p install'
		alias pr='p uninstall'
		alias pre='p reinstall'
		alias pup='p update && brew outdated'
		alias pug='p upgrade'
		alias puu='p update && pug'
		alias pla="gls -ltr --time-style=long-iso /usr/local/Cellar | awk '"'{ print$6 " " $7 " " $8 }'"'"
#elif command -v powerpill >/dev/null 2>&1; then
#		alias p='powerpill'
#		alias pse='p -S --search'
#		alias par="$PSUDO powerpill -Q --deps --unrequired"
#		alias pcl="$PSUDO powerpill -S --clean"
#		alias pdep="$PSUDO powerpill -Q --deps"
#		alias pi="$PSUDO powerpill -S"
#		alias pii="$PSUDO powerpill --noconfirm -S"
#		alias pin='p -S --info'
#		alias pif="$PSUDO powerpill -U"
#		alias pr="$PSUDO powerpill -R --recursive"
#		alias pre='pi'
#		alias pup="$PSUDO pacman -S --refresh --sysupgrade" # pacman used to avoid unsynced mirrors
#		alias pug="$PSUDO pacman -S --sysupgrade"
#		alias puu='pup --noconfirm'
#		alias pla="ls /var/lib/pacman/local/ -ltr --time-style=long-iso | awk '"'{print $6 " " $7 " " $8 " " $9}'"'"
elif command -v pacman >/dev/null 2>&1; then
		alias p='pacman'
		alias pse='p -S --search'
		alias par="$PSUDO pacman -Q --deps --unrequired"
		alias pcl="$PSUDO pacman -S --clean"
		alias pdep="$PSUDO pacman -Q --deps"
		alias pi="$PSUDO pacman -S"
		alias pii="$PSUDO pacman --noconfirm -S"
		alias pin='p -S --info'
		alias pif="$PSUDO pacman -U"
		alias pr="$PSUDO pacman -R --recursive"
		alias pre='pi'
		alias pup="$PSUDO pacman -S --refresh --sysupgrade"
		alias pug="$PSUDO pacman -S --sysupgrade"
		alias puu='pup --noconfirm'
		alias pla="ls /var/lib/pacman/local/ -ltr --time-style=long-iso | awk '"'{print $6 " " $7 " " $8 " " $9}'"'"
elif command -v dnf >/dev/null 2>&1; then
		alias p='dnf'
		alias pse='p search'
		alias par="$PSUDO dnf autoremove"
		alias pbd="$PSUDO dnf builddep || echo Install dnf-plugins-core"
		alias pcl="$PSUDO dnf clean"
		alias pdep="$PSUDO dnf deplist"
		alias pi="$PSUDO dnf install"
		alias pii="$PSUDO dnf -y install"
		alias pin='p info'
		alias pif="$PSUDO rpm -i"
		alias pr="$PSUDO dnf remove"
		alias pre="$PSUDO dnf reinstall"
		alias pup="$PSUDO dnf update-to"
		alias pug="$PSUDO dnf update"
		alias puu='pug'
		alias pla="rpm -qa --qf '%{INSTALLTIME} %{NAME}\n' | sort -n | awk '{a=strftime("'"%Y-%m-%d %H:%M", $1); $1=""; print a $0}'"'"
elif command -v yum >/dev/null 2>&1; then
		alias p='yum'
		alias pse='p search'
		alias par="$PSUDO yum autoremove"
		alias pbd="$PSUDO yum builddep || echo Install dnf-plugins-core"
		alias pcl="$PSUDO yum clean"
		alias pdep="$PSUDO yum deplist"
		alias pi="$PSUDO yum install"
		alias pii="$PSUDO yum -y install"
		alias pin='p info'
		alias pif="$PSUDO rpm -i"
		alias pr="$PSUDO yum remove"
		alias pre="$PSUDO yum reinstall"
		alias pup="$PSUDO yum update-to"
		alias pug="$PSUDO yum update"
		alias puu='pug'
		alias pla="rpm -qa --qf '%{INSTALLTIME} %{NAME}\n' | sort -n | awk '{a=strftime("'"%Y-%m-%d %H:%M", $1); $1=""; print a $0}'"'"
fi
