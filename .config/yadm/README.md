# Setup

Execute:
```
if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit; fi; progs="sudo git wget curl"; for p in aptitude apt apt-get brew dnf yum zypper pkg; do if command -v "$p" >/dev/null 2>&1; then $p update; $p install -y $progs; break; fi; done; if command -v pacman >/dev/null 2>&1; then pacman -Sy --noconfirm; pacman -S --noconfirm $progs; fi; if command -v urpmi >/dev/null 2>&1; then urpmi.update -a; urpmi --force $progs; fi; wget -q https://gitlab.com/jhamfler/dotfiles/raw/master/.config/yadm/init; chmod +x ./init; sh -i -c ./init; rm ./init
```
