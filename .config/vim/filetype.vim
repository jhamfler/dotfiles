" my filetype file
if exists("did_load_filetypes")
	finish
endif
augroup filetypedetect
	au! BufRead,BufNewFile *.zone setfiletype bindzone
	au! BufRead,BufNewFile *.zone set tabstop=8
augroup END
